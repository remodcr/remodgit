﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataBeat.SiteViewModels
{
    public class SiteViewModels
    {
    }
    public class UserType
    {
        public bool ISSiteAdmin { get; set; }
        public bool ISSearchAdmin { get; set; }
        public string UserName { get; set; }
    }
}